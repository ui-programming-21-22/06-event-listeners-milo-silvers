console.log("helooooo")

function pageLoaded(){
    var canvas = document.getElementById("canvas");
    var context = canvas.getContext("2d");
}

pageLoaded()

let divtime = document.getElementById("divtime");
function time(){
    let date = new Date();
    let dd = date.getDate();
    let mm = date.getMonth();
    let yy = date.getFullYear();
    let hh = date.getHours();
    let min = date.getMinutes();
    let ss = date.getSeconds();

    let period = "AM";


    if(hh == 0){
        hh = 12;
    }

    if(hh > 12){
        hh = hh - 12;
        period = "PM";
    }

    console.log("Today's date: "+dd+
        "."+(mm+1)+
        "."+yy+
        " Time: "+hh+
        ":"+min+
        ":"+ss+
        ":" +period)

        divtime.innerHTML = "Today's date: "+dd+
        "."+(mm+1)+
        "."+yy+
        " Time: "+hh+
        ":"+min+
        ":"+ss+ 
        ":" +period;
}

setInterval(time, 1000);

function gameObjects(spritesheet, x, y, width, height){
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width
    this.height = height;
}

function GamerInput(input){
    //input is passed from input functions, left, right etc.
    this.action = input;
}

const canvas = document.getElementById("canvas");
const context = canvas.getContext("2d");

let gamerInput = new GamerInput("None");

let bingusImg = new Image();
bingusImg.src = "assets/bingus.png";
let bingus = new gameObjects(bingusImg, 150, 210, 250, 250);

function gameLoop()
{
    draw();
    update(); 
    requestAnimationFrame(gameLoop);    
}

function draw(){
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage
    (
        bingus.spritesheet, 
        bingus.x,
        bingus.y,
        bingus.width,
        bingus.height
    )
}

function update(){
    playerMVMT();
}

function input(event) {

    if (event.type == "keydown") {

        switch (event.keyCode) {
            case 37:
                gamerInput = new GamerInput("Left");
                break;
            case 65:
                gamerInput = new GamerInput("Left");
                break;
            case 38:
                gamerInput = new GamerInput("Up");
                break; 
            case 87:
                gamerInput = new GamerInput("Up");
                break;
            case 39:
                gamerInput = new GamerInput("Right");
                break;
            case 68:
                gamerInput = new GamerInput("Right");
                break;
            case 40:
                gamerInput = new GamerInput("Down");
                break;
            case 83:
                gamerInput = new GamerInput("Down");
                break;
            case 32:
                console.log("boom");
                document.getElementById('boom').play();
                break;
            default:
                gamerInput = new GamerInput("None");
        }
    }
    else
    {
        gamerInput = new GamerInput("None");
    }
}

function playerMVMT()
{
    if (gamerInput.action == "Up")
    {
        bingus.y -= 5;
    }
    if (gamerInput.action == "Down")
    {
        bingus.y += 5;
    }
    if (gamerInput.action == "Left")
    {
        bingus.x -= 5;
    }
    if (gamerInput.action == "Right")
    {
        bingus.x += 5;
    }

}

window.requestAnimationFrame(gameLoop);
window.addEventListener('keydown',input);
window.addEventListener('keyup',input);